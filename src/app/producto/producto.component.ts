import { ProductoService } from './ProductService';
import { Component, OnInit } from '@angular/core';
import { Producto } from './producto';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  private producto: Producto = new Producto();

  constructor(private productoService: ProductoService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {

  }
  public create(): void {
    this.productoService.create(this.producto).subscribe(producto => {
      this.router.navigate(['/products']);
      Swal.fire(
        'producto creado con exito',
        'producto registrado',
        'success'
      );
  });
  }

}
