export class Producto {
  id: number;
  nombre: string;
  clase: string;
  fecha: Date;
  tope: number;
  registrados: number;
}
