import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Producto } from './producto';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

import { Router } from '@angular/router';


@Injectable()
export class ProductoService {

private urlistar: string = 'http://localhost:8080/product/listar';
private guardar: string = 'http://localhost:8080/product/guardar';
private editar: string = 'http://localhost:8080/product/editar';
private urlEndPointD: string = 'http://localhost:8080/product/eliminar';
private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient, private router: Router) { }

  getProducts(): Observable<Producto[]> {
    //return of(productos);
    return this.http.get(this.urlistar).pipe(
      map(response => response as Producto[])
    );
  }

  create(producto: Producto): Observable<Producto> {
    return this.http.post<Producto>(this.guardar, producto, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        //this.router.navigate(['/producto']);
        console.error(e.error.mensaje);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: e.error.mensaje,
          footer: '<a href>Why do I have this issue?</a>'
        });
        return throwError(e);
      })
      );
  }
  update(producto: Producto): Observable<Producto> {
    return this.http.put<Producto>(this.editar, producto, { headers: this.httpHeaders}).pipe(
    catchError(e => {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: e.error.mensaje,
        footer: '<a href>Why do I have this issue?</a>'
      });
      return throwError(e);
    })
    );
  }
}
