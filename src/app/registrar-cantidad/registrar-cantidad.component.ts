import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../producto/ProductService';
import { Producto } from '../producto/producto';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-cantidad',
  templateUrl: './registrar-cantidad.component.html',
  styleUrls: ['./registrar-cantidad.component.css']
})
export class RegistrarCantidadComponent implements OnInit {

  private producto: Producto = new Producto();

  constructor(private productoService: ProductoService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
  }

  public update(): void {
    this.productoService.update(this.producto).subscribe(producto => {
      Swal.fire(
        'producto actualizado con exito',
        'numero de productos registrado',
        'success'
      );
    });
  }
}
