import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarCantidadComponent } from './registrar-cantidad.component';

describe('RegistrarCantidadComponent', () => {
  let component: RegistrarCantidadComponent;
  let fixture: ComponentFixture<RegistrarCantidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarCantidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarCantidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
