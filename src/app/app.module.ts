import { ProductoService } from './producto/ProductService';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductoComponent } from './producto/producto.component';
import { RegistrarCantidadComponent } from './registrar-cantidad/registrar-cantidad.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ListarComponent } from './listar/listar.component';

export const routes: Routes = [
  {path: '', redirectTo: '/products', pathMatch: 'full'},
  {path: 'RegistrarCantidad', component: RegistrarCantidadComponent},
  {path: 'product', component: ProductoComponent},
  {path: 'listar', component: ListarComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    ProductoComponent,
    RegistrarCantidadComponent,
    ListarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ProductoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
